$(function () {
    $("[data-toggle='tooltip']").tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
      interval: 2000
    })
    $('#contacto').on('show.bs.modal', function(e){
      console.log('El contacto se esta mostrando');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disable',true);
    });
    $('#contacto').on('shown.bs.modal', function(e){
      console.log('El contacto se mostro');
    });
    $('#contacto').on('hide.bs.modal', function(e){
      console.log('El contacto se ocualta');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('El contacto se oculto');
      $('#contactoBtn').prop('disable',false);
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
    });
  })